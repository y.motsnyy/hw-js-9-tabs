"use strict"; 

const tabsTitle = document.querySelectorAll('.tabs-title');
const tabItems = document.querySelectorAll('.tabs-content > li');

tabItems.forEach(elem => elem.classList.add('tab-items'));

document.querySelector('.tabs').addEventListener('click', event => {
  tabsTitle.forEach(elem => elem.classList.remove('active') );
  event.target.classList.add('active');

  tabItems.forEach(elem => {
    if (elem.dataset.f === event.target.dataset.f) {
      elem.classList.add('textDisplay');
    } else {
      elem.classList.remove('textDisplay');
    }
  });
});